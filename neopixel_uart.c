/*
 * neopixel_uart.c
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#include <avr/io.h>
#include <stdint.h>
#include "neopixel_uart.h"

/*
 * Local defines
 */


/*
 * Private functions
 */
static void neopixel_uart_send_byte(uint8_t byte)
{
	for (uint8_t i = 0; i <= 7; i++)
	{
		while(!(LED_USART.STATUS & USART_DREIF_bm)) {
			// Wait for empty buffer
		}
		
		// Write from MSB to LSB
		if (byte & 0x80) {
			LED_USART.TXDATAL = NEOPIXEL_ONE;
		} else {
			LED_USART.TXDATAL = NEOPIXEL_ZERO;
		}
		byte <<= 1;
	}
}

/*
 * Public functions
 */
void neopixel_uart_init(void)
{
	LED_USART.CTRLB = USART_TXEN_bm;
	LED_USART.CTRLC = USART_CMODE_MSPI_gc;
	
	#if ((F_CPU == 24000000ul) || (F_CPU == 20000000ul) || (F_CPU == 16000000ul))
		LED_USART.BAUD = 2 << 6; // F_CPU/4
	#elif ((F_CPU == 12000000ul) || (F_CPU == 10000000ul) || (F_CPU == 8000000ul))
		LED_USART.BAUD = 1 << 6; // F_CPU/2
	#else
		#error "F_CPU needs to be one of the above settings?"
	#endif

	// Update PORTMUX to alternate USART pinout, if used
	#if defined(PORTMUX_CTRLB) // Used by tiny 0- and 1-series
		PORTMUX.CTRLB |= LED_USART_PORT_ALT;  
	#elif defined(PORTMUX_USARTROUTEA) // Used by the rest of the later devices
		#if (defined(PORTMUX_USARTROUTEB) && defined(LED_USART_PORT_ALTB))
			// If using USART4 or USART5 on the large Dx devices
			PORTMUX.USARTROUTEB |= LED_USART_PORT_ALTB;
		#else 
			PORTMUX.USARTROUTEA |= LED_USART_PORT_ALT;
		#endif
	#endif

	// Set Tx to inverted output low
	LED_USART_PORT.OUTSET = LED_DATA_PIN;
	LED_USART_PORT.LED_DATA_PINCTRL = PORT_INVEN_bm;
	LED_USART_PORT.DIRSET = LED_DATA_PIN;
}

void neopixel_uart_configure_single(color_t color)
{
	for(uint8_t idx = 0; idx < NEOPIXEL_LED_BYTES; idx++) {
		neopixel_uart_send_byte(color.array[idx]);
	}
}

void neopixel_uart_configure_array(color_t *array, uint16_t length)
{
	uint8_t *ptr = (uint8_t *) array;
	for(uint16_t idx = 0; idx < length * NEOPIXEL_LED_BYTES; idx++) {
		neopixel_uart_send_byte(ptr[idx]);
	}
}

